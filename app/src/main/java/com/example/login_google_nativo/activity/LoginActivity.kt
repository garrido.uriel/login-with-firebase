package com.example.login_google_nativo.activity

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.login_google_nativo.R
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.*
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private val RC_SIGN_IN: Int = 1203;
    private val TAG: String = "LoginActivity";
    private lateinit var auth: FirebaseAuth;
    private val callbackManager = CallbackManager.Factory.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance();

        configGoogleSignIn()

        configFacebookSignIn()

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun configGoogleSignIn() {
        googleButton.setOnClickListener {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

            val mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            mGoogleSignInClient.signOut();

            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
    }

    private fun configFacebookSignIn() {
        login_button.setPermissions(listOf("email", "public_profile"))
        login_button.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    result?.let {
                        val token = it.accessToken.token
                        firebaseAuth(FacebookAuthProvider.getCredential(token))
                    }
                }

                override fun onCancel() {
                    Toast.makeText(this@LoginActivity, "Operation canceled", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onError(error: FacebookException?) {
                    error?.message?.let { it1 -> Log.e(TAG, it1) }
                }
            })
    }

    private fun firebaseAuth(credential: AuthCredential){
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    Log.w(TAG, "signInWithCredential:failure ${task.exception?.message}", task.exception)
                    val alertDialog = AlertDialog.Builder(this@LoginActivity).create()
                    alertDialog.apply {
                        this.setTitle("Error Login")
                        this.setMessage(task.exception?.message)
                        this.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { dialog, p1 ->
                            dialog.dismiss()
                        }
                    }
                    alertDialog.show()
                }

            }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null && user.email != null) {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("email", user.email)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
                firebaseAuth(GoogleAuthProvider.getCredential(account.idToken!!,null))
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e)
            }
        }
    }

}